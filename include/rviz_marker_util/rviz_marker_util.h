/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/


#ifndef VIZ_UTIL_HPP_R9MLEQ1J
#define VIZ_UTIL_HPP_R9MLEQ1J

#include <Eigen/Core>

#include <ros/ros.h>
#include <ros/console.h>

#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>

namespace ca {
namespace rviz_marker_util {

template<class Scalar>
inline geometry_msgs::Point
MakeGeometryMsgsPoint(Scalar x, Scalar y, Scalar z) {
  geometry_msgs::Point p;
  p.x = x;
  p.y = y;
  p.z = z;
  return p;
}

/**
 * given a bounding box represented as two corners,
 * calculate the lines representing the edges of this box.
 * each line is represented as a pair of points in bb.
 */
template<class Scalar>
void BoundingBoxToLines(const Eigen::Matrix<Scalar, 3, 1>& min_pt,
                        const Eigen::Matrix<Scalar, 3, 1>& max_pt,
                        std::vector<geometry_msgs::Point>& bb) {

  Eigen::Matrix<Scalar, 3, 2> minmax;
  minmax.col(0) = min_pt;
  minmax.col(1) = max_pt;
  static const int seq[3*24] = {
    0,0,0,
    0,1,0,
    0,1,0,
    1,1,0,
    1,1,0,
    1,0,0,
    1,0,0,
    1,0,1,
    1,0,1,
    1,1,1,
    1,1,1,
    0,1,1,
    0,1,1,
    0,0,1,
    0,0,1,
    0,0,0,
    0,1,0,
    0,1,1,
    1,1,1,
    1,1,0,
    0,0,1,
    1,0,1,
    1,0,0,
    0,0,0
  };

  for (int row=0; row < 24; ++row) {
    int i = seq[3*row + 0];
    int j = seq[3*row + 1];
    int k = seq[3*row + 2];
    geometry_msgs::Point pm;
    pm.x = minmax(0, i);
    pm.y = minmax(1, j);
    pm.z = minmax(2, k);
    bb.push_back(pm);
  }

}

/**
 * make a wireframe box marker out of the box represented by min_pt, max_pt
 */
template<class Scalar>
void WireframeBoxMarker(const Eigen::Matrix<Scalar, 3, 1>& min_pt,
                        const Eigen::Matrix<Scalar, 3, 1>& max_pt,
                        visualization_msgs::Marker& bb_marker) {
  bb_marker.ns = "bb";
  bb_marker.id = 0;
  bb_marker.type = visualization_msgs::Marker::LINE_LIST;
  bb_marker.action = visualization_msgs::Marker::ADD;
  bb_marker.scale.x = 0.25;
  bb_marker.color.r = 0.5;
  bb_marker.color.g = 1.0;
  bb_marker.color.b = 0.2;
  bb_marker.color.a = 1.0;
  bb_marker.lifetime = ros::Duration();
  BoundingBoxToLines(min_pt, max_pt, bb_marker.points);
}

}
}
#endif /* end of include guard: VIZ_UTIL_HPP_R9MLEQ1J */
